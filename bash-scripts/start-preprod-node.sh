#!/bin/bash
NODE_CONFIG_DIR=~/cardano-node-8.1.2/preprod

cardano-node run \
--topology $NODE_CONFIG_DIR/topology.json \
--database-path $NODE_CONFIG_DIR/db \
--socket-path $NODE_CONFIG_DIR/node.socket \
--port 3001 \
--config $NODE_CONFIG_DIR/config.json
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- PlutusV2
module AlwaysSucceeds.ValidatorReturnBool (validator) where

import Plutus.V1.Ledger.Value
import Plutus.V2.Ledger.Api
import Plutus.V2.Ledger.Contexts
import Plutus.V1.Ledger.Interval
import PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Plutus.Script.Utils.Typed (mkUntypedValidator)

{-# INLINEABLE alwaysSucceeds #-}
alwaysSucceeds :: BuiltinData -> BuiltinData -> ScriptContext -> Bool
alwaysSucceeds _ _ _ = True

-- {-# INLINEABLE untypedValidator #-}
-- untypedValidator :: BuiltinData -> BuiltinData -> BuiltinData -> ()
-- untypedValidator datum redeemer ctx =
--   check
--     ( myTypedValidator
--         (PlutusTx.unsafeFromBuiltinData datum)
--         (PlutusTx.unsafeFromBuiltinData redeemer)
--         (PlutusTx.unsafeFromBuiltinData ctx)
--     )


validator :: Validator
validator = mkValidatorScript $$(compile [||wrap||])
  -- where wrap d r sc = PlutusTx.Prelude.check $ alwaysSucceeds d r (unsafeFromBuiltinData sc)
  where wrap = mkUntypedValidator alwaysSucceeds
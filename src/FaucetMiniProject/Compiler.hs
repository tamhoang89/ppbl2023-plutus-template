{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module FaucetMiniProject.Compiler (writeFaucetValidatorScript) where

import Cardano.Api
import Codec.Serialise (serialise)
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Plutus.V2.Ledger.Api
import Cardano.Api.Shelley (PlutusScript (..))
import PlutusTx.Prelude
import Prelude (FilePath, IO)

import FaucetMiniProject.Types
import FaucetMiniProject.Validator as Validator

faucetParams :: FaucetParams
faucetParams =
  FaucetParams
    { accessTokenSymbol = "05cf1f9c1e4cdcb6702ed2c978d55beff5e178b206b4ec7935d5e056",
      faucetTokenSymbol = "81209a020cd6d156cb17e0d6e2a48759ca27f2e50390b3176d8a1eb6"
    }

writeValidator :: FilePath -> Plutus.V2.Ledger.Api.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Plutus.V2.Ledger.Api.unValidatorScript

writeFaucetValidatorScript :: IO (Either (FileError ()) ())
writeFaucetValidatorScript = writeValidator "output/my-faucet-script.plutus" $ Validator.validator faucetParams
